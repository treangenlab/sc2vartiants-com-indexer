# HarvestVariants.io Indexer

## Description

HarvestVariants.io Indexer is a tool for generating the core data files utilized by the Harvest Variants Public Genome 
Search Site web application. It takes as input a set of variant call files (each representing a variant genome), a set 
of NCBI SRA "Full XML" metadata files describing the original biological sample of each sequencing run that ultimately 
yielded the variant call files, and a set of Pangolin call files. The tool yields as output index files of a variety of 
features of the variant genomes. (E.g. geographic origin, collection date, Pango lineage, etc.). A second Python script 
is then run to include biocuration commentary into the core data files.

## Example Usage

```
java -jar sc2variants-com-indexer.jar /path/to/sra-xml-files/ /path/to/vcf-files/ /path/to/pango-csv-files/ /out --country-only-ok
python3 gen_vcf_commentary.py /path/to/SARS-CoV-2_variants_mutations_v21_6May2022_final.xlsx /path/to/reference_file.fasta /out
```

The optional `--country-only-ok` flag instructs the program to include entries in the output even if only the sample's 
country can be determined from the XML input and not any more specific geographic origins such as region. 

## Inputs

`/path/to/vcf-files/` - a directory of vcf files to index where each file is named `[sra run id].vcf`

`/path/to/pango-csv-files/` - directory of `[x].csv` Pangolin output files for each input `[x].vcf` file

`/path/to/sra-xml-files/` - NCBI SRA "Full XML" xml files defining the runs that generated the input vcf files

`SARS-CoV-2_variants_mutations_v21_6May2022_final.xlsx` - the biocuration Excel document from the Harvest Variants project

`reference_file.fasta` - the SARS-CoV-2 reference genome

## Outputs

The Java program will create a series of directories:

```
out/genomes/ncbi/sra/.../AAA/BBB/
out/genomes/ncbi/sra/.../CCC/DDD/
...
out/genomes/ncbi/sra/.../YYY/ZZZ/

out/indices/cdclabel
out/indices/country/
out/indices/date/
out/indices/id/
out/indices/locality/
out/indices/month/
out/indices/pango/
out/indices/region/
out/indices/repository/
out/indices/year/
```

The directories starting `out/genomes/ncbi/sra/...` contain JSON descriptions and VCF files of the variant genomes 
organized by run id. The path structure groups data by run id prefix. For example, all run ids starting with 
`DRR286` will be found in the `out/genomes/ncbi/sra/DRR/286/` directory (e.g. run DRR286879 and run DRR286844).

The `indices` directories contain index information about variant genomes in JSON form. For example, the file 
`out/indices/year/2019.txt` contains rows of JSON entries of all samples for variant genomes collected in the year 2019.
Each row in any index JSON file represents one run/variant genome. Similarly, `out/indices/country/angola.txt` lists the
samples collected in Angola.

The second Python program, when run, makes a pass over each VCF file `[x].vcf` below `out/genomes` and creates for each a 
companion file `[x]-commnents.json`. This file contains the relevant biological commentary from
`SARS-CoV-2_variants_mutations_v21_6May2022_final.xlsx` that pertains specifically to the mutations declared in
`[x].vcf`.

## Dependencies

###Java Dependencies
org.json:json:20210307

org.apache.logging.log4j:log4j-api:2.17.2

org.apache.logging.log4j:log4j-core:2.17.2

org.apache.logging.log4j:log4j-slf4j-impl:2.17.2

commons-cli:commons-cli:1.5.0

com.treangenlab.harvest_variants:ncb-sra-full-xml-tools:1.2.0

com.opencsv:com.opencsv:5.3

junit:junit:4.13.2

### Python Dependencies
Pandas 1.4.1

Biopython 1.79

Openpyxl 3.0.9

## Building

From a Maven enabled Bash environment:

``$ ./build.sh``

## Support

This work is supported by the Centers for Disease Control and Prevention (CDC) contract 75D30121C11180.
## License

MIT License

Copyright (c) 2022 Treangen Lab.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.