#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

rm -f $SCRIPT_DIR/sc2variants-com-indexer.jar

mvn -f $SCRIPT_DIR/pom.xml clean package dependency:copy-dependencies
cp $SCRIPT_DIR/target/sc2variants-com-indexer-*-jar-with-dependencies.jar $SCRIPT_DIR/sc2variants-com-indexer.jar

