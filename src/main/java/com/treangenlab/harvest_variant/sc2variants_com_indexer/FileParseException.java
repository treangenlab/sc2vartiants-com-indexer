package com.treangenlab.harvest_variant.sc2variants_com_indexer;

import java.io.File;

/**
 * Thrown to indicate that the contents of a file do not conform to an expected format.
 */
public class FileParseException extends Exception
{
    public final File File;

    public FileParseException(String message, File file)
    {
        super(message + " (from file: " + file.getAbsolutePath() + ")");
        File = file;
    }
}
