package com.treangenlab.harvest_variant.sc2variants_com_indexer;

/**
 * Thrown to indicate the user specified an incorrect number or form of command line arguments.
 */
class BadCommandLineArgumentException extends Exception
{
    BadCommandLineArgumentException(String message)
    {
        super(message);
    }
}
