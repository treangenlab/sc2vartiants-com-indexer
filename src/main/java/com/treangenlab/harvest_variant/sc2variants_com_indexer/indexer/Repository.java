package com.treangenlab.harvest_variant.sc2variants_com_indexer.indexer;

/**
 * The enumerated repositories of bio sample metadata.
 */
enum Repository
{
    NCBI
    {
        @Override
        <R> R which(RepositoryCases<R> cases)
        {
            return cases.ncbiCase();
        }
    };

    abstract <R> R which(RepositoryCases<R> cases);
}
