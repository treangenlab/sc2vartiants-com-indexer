package com.treangenlab.harvest_variant.sc2variants_com_indexer.indexer;

/**
 * A string that does not contain the characters \n or \r.
 */
class LinelessString
{
    private final String _string;

    public LinelessString(String string) throws StringFormatException
    {
        if(string.contains("\n") || string.contains("\r"))
            throw new StringFormatException("string cannot contain newline or carriage return");

        _string = string;
    }

    LinelessString(int value)
    {
        _string = value + "";
    }

    LinelessString(Repository value)
    {
        _string = value.name();
    }

    @Override
    public boolean equals(Object other)
    {
        return other instanceof LinelessString && ((LinelessString)other).equals(this);
    }

    boolean equals(LinelessString other)
    {
        if(other == null)
            return false;

        return other._string.equals(_string);
    }

    @Override
    public int hashCode()
    {
        return _string.hashCode();
    }

    @Override
    public String toString()
    {
        return _string;
    }
}
