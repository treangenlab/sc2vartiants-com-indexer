package com.treangenlab.harvest_variant.sc2variants_com_indexer.indexer;

import com.treangenlab.harvest_variant.sc2variants_com_indexer.FileParseException;
import com.treangenlab.harvest_variant.sc2variants_com_indexer.pango.Pango;
import com.treangenlab.harvest_variant.sc2variants_com_indexer.pango.ProvisioningError;
import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.ExperimentPackageSetNode;
import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.ExperimentPackageSetXmlParser;
import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.ParseException;
import com.treangenlab.harvest_variant.sc2variants_com_indexer.pango.PangoProvider;
import com.treangenlab.harvest_variants.ncbi.sra.geonames.*;
import com.treangenlab.harvest_variants.ncbi.sra.sequenced_sample.SequencedSample;
import com.treangenlab.harvest_variants.ncbi.sra.sequenced_sample.SequencedSampleSet;
import com.treangenlab.harvest_variants.ncbi.sra.sequenced_sample.SequencedSampleSetBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.stream.Collectors;

class IndexerDefault implements Indexer
{
    /**
     * Logger for class.
     */
    private final Logger _logger = LogManager.getLogger(this.getClass());

    private final boolean _allowCountryOnlyGeonameResolution;

    IndexerDefault(boolean allowCountryOnlyGeonameResolution)
    {
        _allowCountryOnlyGeonameResolution = allowCountryOnlyGeonameResolution;
    }

    @Override
    public IndexResult index(File vcfDir, long vcfFileLimit, Set<File> sraExperimentsMetadataXmlFiles,
                             PangoProvider pangoProvider, File outDir)
            throws IOException, FileParseException
    {
        /*
         * Validate method inputs.
         */
        if(vcfDir == null)
            throw new NullPointerException("vcfDir");

        if(vcfFileLimit < 1)
            throw new IllegalArgumentException("vcfFileLimit must be >=1");

        if(sraExperimentsMetadataXmlFiles == null)
            throw new NullPointerException("sraExperimentsMetadataXmlFiles");

        if(outDir == null)
            throw new NullPointerException("outDir");

        for(File dir : new File[] { vcfDir, outDir })
        {
            if(!dir.isDirectory())
                throw new IllegalArgumentException(dir.getAbsolutePath() + " is not a directory");

            if(!dir.exists())
                throw new IllegalArgumentException("Directory does not exist: " + dir.getAbsolutePath());
        }

        /*
         * Create the indices and genomes directories into which this function will write indexes and sample json files.
         */
        File indicesDir = createIndicesDir(outDir);
        File genomesDir = createGenomesDir(indicesDir);

        /*
         * Parse all given input sra xml files and filter the resulting experiment packages down to those packages with
         * only 1 run and 1 sample.
         *
         * We are only interested in experiments that have 1 run and 1 sample because:
         *
         *    1.) 1 run and multiple samples? Well, which sample was used for the run?
         *    2.) 1 sample and multiple runs? Actually, not fatal, but unusual and thus suspicious.
         *    3.) multiple samples and multiple runs? Well, which maps to which?
         *    4.) 0 runs or 0 samples? No way a vcf file can be generated from that and thus be in our program input.
         */
        SequencedSampleSet sequencedSamples;
        int experimentPackageDiscardCount;
        int experimentPackagesTotalCount = 0;
        {
            SequencedSampleSetBuilder sequencedSamplesBuilder = SequencedSampleSetBuilder.make();

            for(File sraXmlFile : sraExperimentsMetadataXmlFiles)
            {
                ExperimentPackageSetNode experimentRoot = parseSraXmlFile(sraXmlFile);
                sequencedSamplesBuilder.addSamplesFromPackagesSkippingInvalid(experimentRoot);
                experimentPackagesTotalCount += experimentRoot.getPackages().size();
            }

            sequencedSamples = sequencedSamplesBuilder.build();
            experimentPackageDiscardCount = sequencedSamplesBuilder.getSkippedSamplesCount();
        }

        /*
         * For every vcf file given as input, attempt to find the original bio sample declared in one of the input xml
         * files (represented now in SequencedSample form in sequencedSamples). If found, create/place an
         * IndexableBioSample corresponding to the xml declaration into samplesForVcfs.
         */
        Set<IndexableBioSample> samplesForVcfs; // the set of samples that generated some (hopefully all) of the vcf
                                                // files named in _vcfDir. a vcf file might not have a corresponding
                                                // sample if we can not create an IndexableBioSample from the run that
                                                // produced the vcf file. e.g. the sample metadata had geodata we
                                                // can't represent.
        int missingSampleCount; // the number of vcf files for which no corresponding sample was found
        int unindexableSamplesCount; // the number of samples matched to a vcf file, but containing unindexable metadata
        int vcfFileCount; // the number of vcf files in _vcfDir
        {
            // Each vcf file will have one associated SRA run id (the filename). Each SequencedSample in
            // sequencedSamples will have one associated SRA run id. Pair vcf files with samples based on the common run
            // id. If a match is found for a given vcf file, add the sample to samplesForVcfs if that sample can be
            // represented as a IndexableBioSample.

            File[] vcfFiles = vcfDir.listFiles(pathname -> pathname.getName().endsWith(".vcf"));
            if(vcfFiles == null)
                throw new IOException("Could not access directory " + vcfDir.getAbsolutePath());

            // the name of the vcf file is the sra run id of the run data that generated the vcf
            List<String> sraRunIds = Arrays.stream(vcfFiles)
                                           .map((File file) -> file.getName().replaceAll(".vcf", ""))
                                           .limit(vcfFileLimit)
                                           .collect(Collectors.toList());

            BuildSampleRecordsResult result = buildSampleRecordsOfRunIds(sraRunIds, pangoProvider, sequencedSamples);
            samplesForVcfs = result.getRecords();

            missingSampleCount = result.getMissingSamplesCount();
            unindexableSamplesCount = result.getUnindexableSamplesCount();
            vcfFileCount = vcfFiles.length;
        }

        /*
         * For each element in samplesForVcfs, include that bio sample in the generated indexes.
         */
        writeIndicesToDirectory(indicesDir, samplesForVcfs);

        /*
         * For each element in samplesForVcfs, write out the sample json file and vcf file.
         */
        for(IndexableBioSample sample : samplesForVcfs)
            writeGenomeFile(sample, genomesDir, vcfDir);

        /*
         * Report results.
         */
        int skippedExperimentPackages = experimentPackageDiscardCount + unindexableSamplesCount;
        // N.b. each sample recorded as skipped by unindexableSamplesCount is derived from a single experiment package.
        // Thus unindexableSamplesCount contributes to skippedExperimentPackages.
        return IndexResult.make(experimentPackagesTotalCount, skippedExperimentPackages, vcfFileCount,
                                missingSampleCount);
    }

    private ExperimentPackageSetNode parseSraXmlFile(File sraXmlFile) throws IOException, FileParseException
    {
        try(FileInputStream inputStream = new FileInputStream(sraXmlFile))
        {
            return ExperimentPackageSetXmlParser.make().parse(inputStream);
        }
        catch (ParseException e)
        {
            throw new FileParseException(e.getMessage(), sraXmlFile);
        }
    }

    private void writeIndicesToDirectory(File indicesDir, Set<IndexableBioSample> samplesForVcfs) throws IOException
    {
        IndexWriter indexWriterManifestOnly = new IndexWriter(indicesDir, true);
        indexWriterManifestOnly.writeIndex(IndexType.Id, samplesForVcfs, IndexableBioSample::getId);

        IndexWriter indexWriterFull = new IndexWriter(indicesDir);
        indexWriterFull.writeIndex(IndexType.Country,    samplesForVcfs, IndexableBioSample::getCountry);
        indexWriterFull.writeIndex(IndexType.Region,     samplesForVcfs, IndexableBioSample::getRegion);
        indexWriterFull.writeIndex(IndexType.Locality,   samplesForVcfs, IndexableBioSample::getLocality);
        indexWriterFull.writeIndex(IndexType.Date,       samplesForVcfs, this::extractCollectionDayOfMonth);
        indexWriterFull.writeIndex(IndexType.Month,      samplesForVcfs, this::extractCollectionMonth);
        indexWriterFull.writeIndex(IndexType.Year,       samplesForVcfs, this::extractCollectionYear);
        indexWriterFull.writeIndex(IndexType.Repository, samplesForVcfs, IndexableBioSample::getRepository);
        indexWriterFull.writeIndex(IndexType.Pango,      samplesForVcfs, IndexableBioSample::getPango);
        indexWriterFull.writeIndex(IndexType.CdcLabel,   samplesForVcfs, IndexableBioSample::getCdcLabel);
    }

    // write the json and vcf from of sample to some subdirectory of genomesDir based on its repository and name.
    private void writeGenomeFile(IndexableBioSample sample, File genomesDir, File vcfDir) throws IOException
    {
        if(genomesDir == null)
            throw new NullPointerException("genomesDir");

        File repoDir = sample.getRepository().which(new RepositoryCases<File>()
        {
            @Override
            public File ncbiCase()
            {
                return new File(new File(genomesDir, "ncbi"), "sra");
            }
        });

        File jsonOutFile = new GenomeOutputFile(sample.getId().toString(), repoDir);
        if(jsonOutFile.exists())
            throw new IllegalStateException(jsonOutFile + " already exists");

        File parentDir = jsonOutFile.getParentFile();
        if(!parentDir.exists() && !parentDir.mkdirs())
            throw new IOException("Could not create directory path for " + parentDir.getAbsolutePath());

        Files.writeString(jsonOutFile.toPath(), sample.toIndexString().toString(), StandardOpenOption.CREATE_NEW);

        File vcfOutFile = new File(jsonOutFile.getCanonicalPath().replace(".json", ".vcf"));
        if(vcfOutFile.exists())
            throw new IllegalStateException(vcfOutFile + " already exists");

        Files.copy(new File(vcfDir, vcfOutFile.getName()).toPath(), vcfOutFile.toPath(),
                   StandardCopyOption.REPLACE_EXISTING); // no existing to replace, but method demands some argument
    }

    private File createIndicesDir(File outDir)
    {
        File dataDir = new File(outDir, "data");

        if (dataDir.exists())
            throw new IllegalStateException(dataDir + " exists");

        if (!dataDir.mkdirs())
            throw new IllegalStateException(dataDir + " could not be created");

        File indicesDir = new File(dataDir, "indices");
        if(!indicesDir.mkdirs())
            throw new IllegalStateException(indicesDir + " could not be created");

        return indicesDir;
    }

    // create a sibling directory to indicesDir named 'genomes'
    //
    // e.g. if given:
    //
    //    /foo/bar/indices/
    //
    // make
    //
    //   /foo/bar/genomes/
    private File createGenomesDir(File indicesDir) throws IOException
    {
        File dataDir = indicesDir.getParentFile();

        if (!dataDir.exists())
            throw new IllegalStateException(dataDir + " does not exist.");

        File genomesDir = new File(dataDir, "genomes");

        if (genomesDir.exists())
            throw new IllegalStateException(genomesDir + " exists.");

        if (!genomesDir.mkdirs())
            throw new IOException(genomesDir + " could not be created");

        return genomesDir;
    }

    private interface BuildSampleRecordsResult { Set<IndexableBioSample> getRecords(); int getMissingSamplesCount();
                                                 int getUnindexableSamplesCount(); }
    private BuildSampleRecordsResult buildSampleRecordsOfRunIds(Iterable<String> sraRunIds, PangoProvider pangoProvider,
                                                                SequencedSampleSet sequencedSamples)
    {
        Map<String, SequencedSample> runIdToSample = buildRunMap(sequencedSamples);

        Set<IndexableBioSample> indexableBioSamples = new HashSet<>();
        int missingSamplesCount = 0;
        int unindexableSamplesCount = 0;
        for (String sraRunId : sraRunIds)
        {
            SequencedSample sampleWithRunId = runIdToSample.get(sraRunId);
            if(sampleWithRunId == null)
            {
                missingSamplesCount++;
                continue;
            }

            IndexableBioSample indexableSample;
            try
            {
                indexableSample = buildSample(sampleWithRunId, pangoProvider);
            }
            catch (GeoExtractionException e)
            {
                unindexableSamplesCount++;
                _logger.warn(e.getMessage(), e);
                _logger.warn("Geoextraction failed for run id " + sraRunId);
                continue;
            }
            catch (StringFormatException e)
            {
                unindexableSamplesCount++;
                _logger.warn("StringFormatException for run id " + sraRunId, e);
                continue;
            }
            catch (ProvisioningError e)
            {
                unindexableSamplesCount++;
                _logger.warn("Pango operation exception for run id " + sraRunId, e);
                continue;
            }

            indexableBioSamples.add(indexableSample);
        }

        int missingSamplesCountCapture = missingSamplesCount;
        int unindexableSamplesCountCapture = unindexableSamplesCount;
        return new BuildSampleRecordsResult()
        {
            @Override
            public Set<IndexableBioSample> getRecords()
            {
                return Collections.unmodifiableSet(indexableBioSamples);
            }

            @Override
            public int getMissingSamplesCount()
            {
                return missingSamplesCountCapture;
            }

            @Override
            public int getUnindexableSamplesCount()
            {
                return unindexableSamplesCountCapture;
            }
        };
    }

    private IndexableBioSample buildSample(SequencedSample sample, PangoProvider pangoProvider)
            throws StringFormatException, GeoExtractionException, ProvisioningError
    {
        LinelessString id = new LinelessString(sample.getRunId());
        Optional<Pango> pango = pangoProvider.getPangoOfSample(sample.getRunId());

        String keywords = sample.getPrimaryGeoTag().orElse("") + ":" + sample.getSecondaryGeoTag().orElse("");

        GeoExtractor.Result sampleLocation = new SraGeoExtractor(_allowCountryOnlyGeonameResolution).extract(keywords);

        Country country = sampleLocation.getCountry().orElse(null);
        Region region = sampleLocation.getRegion().orElse(null);
        Locality locality = sampleLocation.getLocality().orElse(null);

        LocalDate collectionDate;
        {
            String date = sample.getCollectionDate().orElse(null);
            if(date == null)
            {
                collectionDate = null;
            }
            else
            {
                try
                {
                    collectionDate = LocalDate.parse(date);
                }
                catch (DateTimeParseException e)
                {
                    collectionDate = null;
                }
            }
        }

        LinelessString countryString  = country  == null ? null : new LinelessString(country.getName());
        LinelessString regionString   = region   == null ? null : new LinelessString(region.getName());
        LinelessString localityString = locality == null ? null : new LinelessString(locality.getName());
        return new IndexableBioSample(id, Repository.NCBI, countryString, regionString, localityString,
                                      pango.orElse(null), collectionDate);

    }

    private Map<String, SequencedSample> buildRunMap(SequencedSampleSet sequencedSamples)
    {
        Map<String, SequencedSample> runIdToSample = new HashMap<>();
        for(SequencedSample sample : sequencedSamples)
        {
            String runId = sample.getRunId();

            if(runIdToSample.containsKey(runId))
                throw new RuntimeException("Run " + runId + " mapped to multiple samples");

            runIdToSample.put(runId, sample);
        }

        return runIdToSample;
    }

    private Integer extractCollectionDayOfMonth(IndexableBioSample record)
    {
        LocalDate collectionDate = record.getCollectionDate().orElse(null);
        if(collectionDate == null)
            return null;

        return collectionDate.getDayOfMonth();
    }

    private Integer extractCollectionMonth(IndexableBioSample record)
    {
        LocalDate collectionDate = record.getCollectionDate().orElse(null);
        if(collectionDate == null)
            return null;

        return collectionDate.getMonth().getValue();
    }

    private Integer extractCollectionYear(IndexableBioSample record)
    {
        LocalDate collectionDate = record.getCollectionDate().orElse(null);
        if(collectionDate == null)
            return null;

        return collectionDate.getYear();
    }
}