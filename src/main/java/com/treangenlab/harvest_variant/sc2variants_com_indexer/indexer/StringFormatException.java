package com.treangenlab.harvest_variant.sc2variants_com_indexer.indexer;

/**
 * Thrown to indicate a string is not in the expected format.
 */
class StringFormatException extends Exception
{
    public StringFormatException(String msg)
    {
        super(msg);
    }
}
