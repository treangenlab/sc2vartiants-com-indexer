package com.treangenlab.harvest_variant.sc2variants_com_indexer.indexer;

import com.treangenlab.harvest_variant.sc2variants_com_indexer.FileParseException;
import com.treangenlab.harvest_variant.sc2variants_com_indexer.pango.PangoProvider;

import java.io.File;
import java.io.IOException;
import java.util.Set;

public interface Indexer
{
    IndexResult index(File vcfDir, long vcfFileLimit, Set<File> sraExperimentsMetadataXmlFiles,
                      PangoProvider pangoProvider, File outDir)
            throws IOException, FileParseException;

    static Indexer make(boolean allowCountryOnlyGeonameResolution)
    {
        return new IndexerDefault(allowCountryOnlyGeonameResolution);
    }

}


