package com.treangenlab.harvest_variant.sc2variants_com_indexer.indexer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


class GenomeOutputFile extends File
{
    /**
     * A file that ends in {@code id}.json, is descended from {@code ancestorDir}, and has path segments based on the
     * id grouped by every three characters but excluding the final group.
     *
     * For example, if constructed with id "123456789" and the ancestor /foo/bar, would be the file:
     *
     *     /foo/bar/123/456/123456789.json
     *
     * For example, if constructed with id "123456789AB" and the ancestor /foo/bar, would be the file:
     *
     *    /foo/bar/123/456/789/123456789AB.json
     */
    GenomeOutputFile(String id, File ancestorDir)
    {
        super(calculateParentDir(id, ancestorDir), id + ".json");
    }

    private static File calculateParentDir(String id, File ancestorDir)
    {
        List<String> idSegments;
        {
            List<String> chunksAccum = new ArrayList<>();
            for (int i = 0; i < id.length(); i += 3)
            {
                String chunk = id.substring(i, Math.min(id.length(), i+3));
                chunksAccum.add(chunk);
            }

            idSegments = chunksAccum.subList(0, chunksAccum.size()-1);
        }


        File parentDirAccum = ancestorDir;
        for(String dirPart : idSegments)
            parentDirAccum = new File(parentDirAccum, dirPart);

        return parentDirAccum;
    }
}
