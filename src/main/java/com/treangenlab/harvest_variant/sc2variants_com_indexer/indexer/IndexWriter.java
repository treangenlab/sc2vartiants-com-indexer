package com.treangenlab.harvest_variant.sc2variants_com_indexer.indexer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

class IndexWriter
{
    interface LinelessSelector extends Function<IndexableBioSample, LinelessString> {}

    interface OptionalLinelessSelector extends Function<IndexableBioSample, Optional<LinelessString>> {}

    interface IntSelector extends Function<IndexableBioSample, Integer> {}

    interface RepoSelector extends Function<IndexableBioSample, Repository> {}

    private final File _parentDir;

    private final boolean _onlyWriteManifestFile;

    IndexWriter(File parentDir)
    {
        this(parentDir, false);
    }

    IndexWriter(File parentDir, boolean onlyWriteManifestFile)
    {
        if(parentDir == null)
            throw new NullPointerException("parentDir");

        _parentDir = parentDir;
        _onlyWriteManifestFile = onlyWriteManifestFile;
    }

    void writeIndex(IndexType type, Set<IndexableBioSample> records, IntSelector sampleFieldSelector) throws IOException
    {
        writeIndex(type, records, (IndexableBioSample record) ->
        {
            Integer field = sampleFieldSelector.apply(record);
            return field == null ? null : new LinelessString(field);
        }, true);
    }

    void writeIndex(IndexType type, Set<IndexableBioSample> records, RepoSelector sampleFieldSelector) throws IOException
    {
        writeIndex(type, records, (IndexableBioSample record) -> new LinelessString( sampleFieldSelector.apply(record)),
                   false);
    }

    void writeIndex(IndexType type, Set<IndexableBioSample> records, OptionalLinelessSelector sampleFieldSelector)
            throws IOException
    {
        writeIndex(type, records, (IndexableBioSample record) ->
        {
            Optional<LinelessString> field = sampleFieldSelector.apply(record);
            return field.isEmpty() ? null : field.get();
        }, true);
    }

    void writeIndex(IndexType type, Set<IndexableBioSample> records, LinelessSelector sampleFieldSelector)
            throws IOException
    {
        writeIndex(type, records, sampleFieldSelector, false);
    }

    // create and populate all the files of an index of the given type. e.g. (for locality type)
    //
    // /dir/data/locality/_.txt
    // /dir/data/locality/houston.txt
    // /dir/data/locality/dallas.txt
    // ...
    private void writeIndex(IndexType type, Set<IndexableBioSample> records, LinelessSelector sampleFieldSelector,
                            boolean fieldSelectorMayReturnNull) throws IOException
    {
        File indexDir = new File(_parentDir, type.name().toLowerCase()); // e.g. `/dir/data/locality/`
        if(!indexDir.mkdirs())
            throw new IOException("Could not make directory " + indexDir);

        File manifestFile = new File(indexDir, "_.txt"); // e.g. `/dir/data/locality/_.txt`

        /*
         * For each record in `records` apply `sampleFieldSelector` to extract the feature value corresponding to `type`
         * from the record (if present).
         *
         * If the feature value is present, perform two actions:
         *
         * 1. Insert the feature value into the set of all extracted feature values from all records.
         *    (i.e. insert to `distinctRecordFieldValuesAccum`).
         *
         * 2.) If `_onlyWriteManifestFile` is false, append the record in string form to a file (approximately) named
         *     `[feature_value].txt`
         */
        Set<String> distinctRecordFieldValuesAccum = new HashSet<>(); // the set of values to write to `_.txt`
        for(IndexableBioSample record : records)
        {
            String featureValueLowerCase;
            {
                LinelessString linelessEntry = sampleFieldSelector.apply(record);
                if(linelessEntry == null && fieldSelectorMayReturnNull)
                    continue; // no feature value for `type` on `record` (and that is permissible), so skip
                featureValueLowerCase = linelessEntry.toString().toLowerCase();
            }
            distinctRecordFieldValuesAccum.add(featureValueLowerCase);

            if(_onlyWriteManifestFile)
                continue;

            /*
             * Append the string version of record to the index file corresponding to `featureValue`.
             *
             * For example if `type` represents locality and `record`'s locality is Houston, then append `record`'s
             * string representation to the index file ending in `houston.txt`.
             */
            File indexFile = // e.g. `houston.txt`
                    new File(indexDir, featureValueLowerCase.replaceAll(" ", "_").replaceAll(":", "_") + ".txt");
            if(!indexFile.exists())
                if(!indexFile.createNewFile())
                    throw new IOException("Could not create file " + indexFile);

            Files.writeString(indexFile.toPath(), record.toIndexString().toString() + "\n", StandardOpenOption.APPEND);

        }

        /*
         * Write manifest file for index.
         */
        Files.writeString(manifestFile.toPath(),
                          String.join("\n", distinctRecordFieldValuesAccum),StandardOpenOption.CREATE_NEW);
    }
}
