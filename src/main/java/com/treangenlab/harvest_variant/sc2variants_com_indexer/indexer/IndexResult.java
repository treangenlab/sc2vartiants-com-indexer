package com.treangenlab.harvest_variant.sc2variants_com_indexer.indexer;

public interface IndexResult
{
    long getExperimentPackagesCount();

    long getSkippedExperimentPackagesCount();

    long getVcfFilesCount();

    long getVcfFilesWithNoCorrespondingExperimentsCount();

    static IndexResult make(long experimentPackagesCount, long skippedExperimentPackagesCount, long vcfFilesCount,
                            long vcfFilesWithNoCorrespondingExperimentsCount)
    {
        return new IndexResult()
        {
            @Override
            public long getExperimentPackagesCount()
            {
                return experimentPackagesCount;
            }

            @Override
            public long getSkippedExperimentPackagesCount()
            {
                return skippedExperimentPackagesCount;
            }

            @Override
            public long getVcfFilesCount()
            {
                return vcfFilesCount;
            }

            @Override
            public long getVcfFilesWithNoCorrespondingExperimentsCount()
            {
                return vcfFilesWithNoCorrespondingExperimentsCount;
            }
        };
    }
}
