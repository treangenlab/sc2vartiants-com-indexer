package com.treangenlab.harvest_variant.sc2variants_com_indexer.indexer;

interface RepositoryCases<R>
{
    R ncbiCase();
}
