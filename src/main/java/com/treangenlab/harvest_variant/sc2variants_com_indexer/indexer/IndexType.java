package com.treangenlab.harvest_variant.sc2variants_com_indexer.indexer;

/**
 * Models the features of a bio sample's metadata that are eligible for indexing.
 */
enum IndexType { Country, Region, Locality, Date, Month, Year, Id, Repository, Pango, CdcLabel }