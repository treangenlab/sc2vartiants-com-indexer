package com.treangenlab.harvest_variant.sc2variants_com_indexer;

import com.treangenlab.harvest_variant.sc2variants_com_indexer.indexer.IndexResult;
import com.treangenlab.harvest_variant.sc2variants_com_indexer.indexer.Indexer;
import com.treangenlab.harvest_variant.sc2variants_com_indexer.pango.PangoProvider;
import com.treangenlab.harvest_variant.sc2variants_com_indexer.pango.PangoProviderCsvDir;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * The class containing the program entry point.
 */
public class Program
{
    private static final String _countryOnlyOkFlag = "--country-only-ok";

    private static final String _limitFlag = "--limit";

    static
    {
        /*
         * Initialize logging system.
         */
        String log4jPropertyKey = "log4j.configurationFile";
        if(!System.getProperties().containsKey(log4jPropertyKey))
            System.setProperty(log4jPropertyKey, "log4j2.xml"); // unless given a specific config, assume default

        /*
         * Log any unhandled exceptions and terminate if encountered.
         */
        Thread.setDefaultUncaughtExceptionHandler((Thread t, Throwable e) ->
        {
            try
            {
                LogManager.getLogger(Program.class).fatal(e.getMessage(), e);
            }
            catch (Throwable ignore)
            {
                // eat un-loggable exceptions
            }

            e.printStackTrace(System.err);
            System.exit(1);
        });
    }

    public static void main(String[] args)
    {
        Logger logger = LogManager.getLogger(Program.class);
        logger.debug("entering");

        try
        {
            mainHelp(args);
        }
        catch (IOException e)
        {
            logger.fatal(e.getMessage(), e);
            System.err.println(e.getMessage());
            e.printStackTrace(System.err);
            logger.debug("exiting");
            System.exit(3);
        }
        catch (BadCommandLineArgumentException e)
        {
            logger.fatal(e.getMessage(), e);
            System.err.println(e.getMessage());
            e.printStackTrace(System.err);
            logger.debug("exiting");
            System.exit(4);
        }
        catch (FileParseException e)
        {
            logger.fatal(e.getMessage(), e);
            System.err.println(e.getMessage());
            e.printStackTrace(System.err);
            logger.debug("exiting");
            System.exit(5);
        }

        logger.debug("exiting");
    }

    // a helper for main that is allowed to throw checked exceptions
    public static void mainHelp(String[] args) throws IOException, BadCommandLineArgumentException, FileParseException
    {
        Logger logger = LogManager.getLogger(Program.class);
        logger.debug("entering");

        if (args.length == 4 || args.length == 5 || args.length == 6)
        {
            Set<File> sraExperimentsMetadataXmlFiles = determineSraXmlFilesFromArgs(args[0]);
            File vcfDir = new File(args[1]);
            File pangoDir = new File(args[2]);
            File outDir = new File(args[3]);

            boolean allowCountryOnlyGeonameResolution = Arrays.asList(args).contains(_countryOnlyOkFlag);
            long maxVcfsToRead = Arrays.asList(args).contains(_limitFlag) ? 5000 : Long.MAX_VALUE;

            PangoProvider pangoRepo = new PangoProviderCsvDir(pangoDir);
            Indexer indexer = Indexer.make(allowCountryOnlyGeonameResolution);
            IndexResult result = indexer.index(vcfDir, maxVcfsToRead, sraExperimentsMetadataXmlFiles, pangoRepo,outDir);

            System.out.println(result.getExperimentPackagesCount() + " experiments packages found.");
            System.out.println(result.getSkippedExperimentPackagesCount() +
                    " experiments packages skipped (bad XML or context errors).");
            System.out.println(result.getVcfFilesCount() + " vcf files found.");
            System.out.println(result.getVcfFilesWithNoCorrespondingExperimentsCount() +
                    " vcf files with no corresponding bio samples.");
        }
        else
        {
            showUsage();
            logger.debug("exiting");
            System.exit(2);
        }

        logger.debug("exiting");
    }

    private static void showUsage()
    {
        Logger logger = LogManager.getLogger(Program.class);
        logger.debug("entering");

        System.out.println("Usage:");
        System.out.println("java [-Dlog4j.configurationFile=CONFIG.XML] -jar sc2variants-com-indexer.jar " +
                           "SRA_METADATA_XML_FILE_OR_XMLS_DIR VCF_DIR PANGO_DIR OUT_DIR [" + _limitFlag + "] " +
                           "[" + _countryOnlyOkFlag + "]");
        System.out.println();
        System.out.println("Options:");
        System.out.println();
        System.out.println("  " + _limitFlag + "              limits vcf inputs to first 5000");
        System.out.println("  " + _countryOnlyOkFlag + "    allow index outputs of country geography only");
        System.out.println();

        logger.debug("exiting");
    }


    // determine if arg[0] is a file or directory. If a file, return a singleton set of that file. If a directory,
    // return all the xml files in the directory (non-recursive).
    private static Set<File> determineSraXmlFilesFromArgs(String arg0) throws BadCommandLineArgumentException,
                                                                                IOException
    {
        Logger logger = LogManager.getLogger(Program.class);
        logger.debug("entering");

        if(arg0 == null)
        {
            logger.warn("arg0 is null");
            logger.debug("exiting");
            throw new NullPointerException("arg0");
        }

        File xmlFileArg = new File(arg0); // could be a specific file to parse or a directory of files to parse
        if(!xmlFileArg.exists())
        {
            logger.warn(xmlFileArg.getAbsolutePath() + " does not exist.");
            logger.debug("exiting");
            throw new BadCommandLineArgumentException(xmlFileArg.getAbsolutePath() + " does not exist.");
        }

        /*
         * Determine the list of files to process.
         */
        File[] sraExperimentsMetadataXmlFiles;
        if(xmlFileArg.isDirectory())
        {
            sraExperimentsMetadataXmlFiles = xmlFileArg.listFiles((dir, name) -> name.endsWith(".xml"));

            if(sraExperimentsMetadataXmlFiles == null)
            {
                logger.warn("sraExperimentsMetadataXmlFiles null");
                logger.debug("exiting");
                throw new IOException("Could not read from " + xmlFileArg.getAbsolutePath());
            }

            if(sraExperimentsMetadataXmlFiles.length == 0)
            {
                logger.warn(xmlFileArg.getAbsolutePath() + " empty");
                logger.debug("exiting");
                throw new BadCommandLineArgumentException("No xml files found to parse in " +
                        xmlFileArg.getAbsolutePath());
            }
        }
        else if(xmlFileArg.isFile())
        {
            sraExperimentsMetadataXmlFiles = new File[]{xmlFileArg};
        }
        else
        {
            logger.warn(xmlFileArg.getAbsolutePath() + " malformed");
            logger.debug("exiting");
            throw new BadCommandLineArgumentException(xmlFileArg.getAbsolutePath() + " is neither a directory or file");
        }

        Set<File> result = new HashSet<>(Arrays.asList(sraExperimentsMetadataXmlFiles));

        logger.debug("exiting");
        return result;
    }
}
