package com.treangenlab.harvest_variant.sc2variants_com_indexer.pango;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * A provider based on a directory of CSV files where each file is produced by Pangolin and named {@code [run id].csv}.
 */
public class PangoProviderCsvDir implements PangoProvider
{
    /**
     * Logger for class
     */
    Logger _logger = LogManager.getLogger(PangoProviderCsvDir.class);

    /**
     * The directory containing all the csv files.
     */
    private final File _csvDir;

    public PangoProviderCsvDir(File csvDir)
    {
        _logger.debug("entering");

        if(csvDir == null)
        {
            _logger.warn("csvDir null");
            _logger.debug("exiting");
            throw new NullPointerException("csvDir");
        }

        if(!csvDir.exists())
        {
            _logger.warn(csvDir.getAbsolutePath() + " does not exist.");
            _logger.debug("exiting");
            throw new IllegalArgumentException(csvDir.getAbsolutePath() + " does not exist.");
        }

        if(!csvDir.isDirectory())
        {
            _logger.warn(csvDir.getAbsolutePath() + " does not exist.");
            _logger.debug("exiting");
            throw new IllegalArgumentException(csvDir.getAbsolutePath() + " is not a directory.");
        }

        _csvDir = csvDir;

        _logger.debug("exiting");
    }

    @Override
    public Optional<Pango> getPangoOfSample(String runId) throws ProvisioningError
    {
        _logger.debug("entering");

        File pangolinOutputCsvFile = new File(_csvDir, runId.toUpperCase() + ".csv");
        if(!pangolinOutputCsvFile.exists())
        {
            _logger.warn("No such file " + pangolinOutputCsvFile.getAbsolutePath());
            _logger.debug("exiting");
            throw new ProvisioningError("No such file " + pangolinOutputCsvFile.getAbsolutePath());
        }

        List<String[]> pangolinOutputCsvFileLines;
        try(FileReader pangolinOutputCsvFileReader = new FileReader(pangolinOutputCsvFile))
        {
            try(CSVReader csvReader = new CSVReader(pangolinOutputCsvFileReader))
            {
                pangolinOutputCsvFileLines = csvReader.readAll();
            }
            catch (CsvException e)
            {
                _logger.warn(e.getMessage(), e);
                _logger.debug("exiting");
                throw new ProvisioningError(e);
            }
        }
        catch (IOException e)
        {
            _logger.warn(e.getMessage(), e);
            _logger.debug("exiting");
            throw new ProvisioningError(e);
        }

        if(pangolinOutputCsvFileLines.size() != 2)
        {
            _logger.warn("Expected " + pangolinOutputCsvFile.getAbsolutePath() +
                    " file to be two lines long.");
            _logger.debug("exiting");
            throw new ProvisioningError("Expected " + pangolinOutputCsvFile.getAbsolutePath() +
                    " file to be two lines long.");
        }

        String[] headerLine = pangolinOutputCsvFileLines.get(0);

        if(!String.join(",",headerLine).startsWith("taxon,lineage"))
        {
            _logger.warn("Unexpected csv header in " + pangolinOutputCsvFile.getAbsolutePath());
            _logger.debug("exiting");
            throw new ProvisioningError("Unexpected csv header in " + pangolinOutputCsvFile.getAbsolutePath());
        }

        String[] valuesLine = pangolinOutputCsvFileLines.get(1);

        if(valuesLine.length < 2)
        {
            _logger.warn("Expected additional row values in " +
                    pangolinOutputCsvFile.getAbsolutePath());
            _logger.debug("exiting");
            throw new ProvisioningError("Expected additional row values in " +
                    pangolinOutputCsvFile.getAbsolutePath());
        }

        String lineage = valuesLine[1];

        Optional<Pango> result;
        if(lineage.equals("None") || lineage.equals("Unassigned"))
        {
            result = Optional.empty();
        }
        else if(Pango.isLineage(lineage))
        {
            result = Optional.of(Pango.parse(lineage));
        }
        else
        {
            _logger.warn("Unknown lineage " + lineage);
            _logger.debug("exiting");
            throw new ProvisioningError("Unknown lineage " + lineage);
        }

        _logger.debug("exiting");
        return result;
    }
}
