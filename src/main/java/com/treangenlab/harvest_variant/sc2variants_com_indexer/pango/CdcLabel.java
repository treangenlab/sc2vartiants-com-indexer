package com.treangenlab.harvest_variant.sc2variants_com_indexer.pango;

public enum CdcLabel
{
    Alpha, Beta, Gamma, Epsilon, Eta, Iota, Kappa, Mu, Zeta,
    Delta, Omicron
}
