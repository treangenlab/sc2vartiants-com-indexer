package com.treangenlab.harvest_variant.sc2variants_com_indexer.pango;

/**
 * Thrown to indicate an unexpected error in reporting a Pango Linage for a sample.
 */
public class ProvisioningError extends Exception
{
    public ProvisioningError(String message)
    {
        super(message);
    }

    public ProvisioningError(Exception cause)
    {
        super(cause);
    }
}
