package com.treangenlab.harvest_variant.sc2variants_com_indexer.pango;

import java.util.Optional;

/**
 * A provider that knows the Pango Lineages of biological samples.
 */
public interface PangoProvider
{
    /**
     * Returns the Pango Lineage of the biological sample with the given run id.
     *
     * @param runId the run id of the run using the biological sample.
     * @return the pango linage of the biological sample, or {@code empty} if the sample's linage was declared as
     *         "None" by the Pangolin software.
     * @throws ProvisioningError if the given run id is unknown or other unexpected errors occur when retrieving
     *         linage information.
     */
    Optional<Pango> getPangoOfSample(String runId) throws ProvisioningError;
}
