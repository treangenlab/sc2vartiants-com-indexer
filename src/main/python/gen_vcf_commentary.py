import glob
import json
import sys

import pandas as pd
from Bio import SeqIO

from nt_aa_mapping import get_proteins_pos_dict, get_pos_protein_dict, query_nt_mut


def extract_comment_list(data_frame):
    """
    Gathers all the values of the data frame that occur at the second column and returns them in list form
    """
    comment_list = []

    commentary_key = data_frame.columns[1]
    comment_count = len(data_frame[commentary_key].values)
    for i in range(comment_count):
        comment_list.append(data_frame[commentary_key].values[i])

    return comment_list


# For every ".vcf" file found in or below the data directory path (command line argument 3), generate a corresponding
# "-comments.json" file of the same name in the same directory that contains applicable bio curation commentary for the
# vcf file's declared mutations. The source of the bio curation commentary is an Excel file that is specified by
# command line argument 1. Argument 2 is expected to be the reference genome fasta file for SARS-Cov-2.


if __name__ == "__main__":
    biocurated_xlsx_f = sys.argv[1]
    reference_fasta_f = sys.argv[2]
    data_dir_path = sys.argv[3]

    # load input files
    biocurated_xlsx = pd.read_excel(biocurated_xlsx_f, sheet_name=None)
    reference = SeqIO.read(reference_fasta_f, "fasta")

    protein_pos_dict = get_proteins_pos_dict(biocurated_xlsx)  # mapping of protein id to zero-index on reference genome
    pos_protein_dict = get_pos_protein_dict(protein_pos_dict)  # mapping of zero-index position on ref to protein id

    #
    # Create a dictionary keyed by protein id that contains commentary about mutations that can be found in that
    # protein (all in the amino acid space).
    #
    biocuration_pd_dict = dict()
    for protein_id in protein_pos_dict:
        try:
            aa_mut_df = biocurated_xlsx[protein_id].copy()
            aa_mut_df = aa_mut_df[pd.to_numeric(aa_mut_df['Position'], errors='coerce').notnull()]
            aa_mut_df['Position'] = aa_mut_df['Position'].astype(int)
            aa_mut_df.set_index('Position', inplace=True)
            biocuration_pd_dict[protein_id] = aa_mut_df
        except KeyError as err:
            continue

    #
    # For each file found in "/path/to/x.vcf", create a file "/path/to/x-comments.json" that contains a JSON object
    # keyed by nucleotide position numbers (encoded as strings) with information about the mutation declared in the vcf
    # file.
    #
    for vcf_file_path in glob.glob(data_dir_path + '/**/*.vcf', recursive=True):
        vcf_file = open(vcf_file_path, 'r')
        vcf_lines = vcf_file.readlines()

        #
        # Create a dictionary that maps a nucleotide position key to a dictionary of information about that mutation
        # as described in the comment in the inner loop. e.g.
        # {
        #  "9777"  : { ... },
        #  "11287" : { ... }
        # }
        #
        commentary_dict = {}
        for line in vcf_lines:
            if line.startswith('#'):
                continue
            line_parts = line.split()
            pos = int(line_parts[1])
            ref = line_parts[3]
            alt = line_parts[4]

            #
            # Create a dictionary that contains information about the mutation at the position `pos`: e.g.
            #
            # {
            #  "ref": "GTCTGGTTTT",
            #  "alt": "G",
            #  "proteinId": "Nsp6",
            #  "mutations": "Ser106-,Gly107-,Phe108-",
            #  "directMutations": [
            #     "The L105Δ-S106Δ-G107Δ mutant is found in the omicron variant.",
            #     "The S106Δ mutant in Nsp6 is found in the alpha, beta, gamma, eta, and iota [PMID34535792]"
            #   ],
            #   "supplementaryMutations": []
            # }
            #
            pos_comments_dict = {"ref": ref, "alt": alt}
            try:
                direct_mutations, sup_mutations, protein_id, mutations = \
                    query_nt_mut(pos, ref, alt, reference, pos_protein_dict, protein_pos_dict, biocuration_pd_dict)
            except TypeError:
                continue
            pos_comments_dict["proteinId"] = protein_id
            pos_comments_dict["mutations"] = mutations

            if not direct_mutations.empty:
                pos_comments_dict["directMutations"] = extract_comment_list(direct_mutations)
            else:
                pos_comments_dict["directMutations"] = []

            if not sup_mutations.empty:
                pos_comments_dict["supplementaryMutations"] = extract_comment_list(sup_mutations)
            else:
                pos_comments_dict["supplementaryMutations"] = []

            commentary_dict[pos] = pos_comments_dict

        # convert commentary_dict to JSON and write to "/path/x-comments.json" where the motivating vcf is "/path/x.vcf"
        comments_file_path = vcf_file_path.replace('.vcf', '-comments.json')
        with open(comments_file_path, "w") as outfile:
            json.dump(commentary_dict, outfile)
