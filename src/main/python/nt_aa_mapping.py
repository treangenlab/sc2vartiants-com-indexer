from collections import defaultdict
import re

import pandas as pd
from Bio import SeqIO
from Bio.Seq import translate
from Bio.SeqUtils import seq3


# Dependencies
# conda install -c conda-forge biopython
# conda install -c anaconda pandas

def get_proteins_pos_dict(biocurated_xlsx):
    '''
    This function takes the biocurated information from xlsx and returns
    A dictinary of mapping from each protein_id to its positions (zero-indexed) on reference genome
    '''
    proteins_df = biocurated_xlsx['Proteins'].set_index('Sequence').copy()
    proteins_df = proteins_df[proteins_df.index.notnull()]
    proteins_df = proteins_df.rename(columns={'Position in WIV04 genome [PMID33347989]': 'NT_Range'},
                                     index={'Surface/Spike (S) glycoprotein': 'S',
                                            'Nucleocapsid': 'N',
                                            'Envelope': 'E',
                                            'Membrane': 'M'})
    temp_df = proteins_df.dropna(axis=0)
    protein_pos_dict = defaultdict(list)

    for index, row in temp_df.iterrows():
        # Need to double check Nsp12, since Nsp12 contains merging range 13442-13468 and 13468-16236
        protein_nt_ranges = row['NT_Range'].split("|")
        for protein_nt_range in protein_nt_ranges:
            start = int(protein_nt_range.split("-")[0]) - 1
            end = int(protein_nt_range.split("-")[-1])
            protein_pos_dict[index].append((start, end))

    return protein_pos_dict


def get_pos_protein_dict(protein_pos_dict):
    '''
    This function takes the mapping from protein_id to pos (zero-indexed) and returns
    a mapping from pos (zero-indexed) to protein_id
    '''
    pos_protein_dict = dict()
    for protein_id in protein_pos_dict:
        if len(protein_pos_dict[protein_id]) == 1:
            start, end = protein_pos_dict[protein_id][0]
        else:
            start, end = protein_pos_dict[protein_id][0][0], protein_pos_dict[protein_id][-1][-1]
        for i in range(start, end):
            pos_protein_dict[i] = protein_id

    return pos_protein_dict


def nt_to_aa(pos, ref, alt, reference, pos_protein_dict, protein_pos_dict):
    '''
    This function takes pos(int), ref(string), alt(string) follows the lofreq output VCF format and returns
      1) the protein_id,
      2) the associated amino acid mutations in list.
    The amino acid mutation is formatted as:
      1) SNP, [REF_AA][POS][ALT_AA]
      2) DEL, [REF_AA][POS][-], multiple consecutive amino acid deletions are annotated seperately
      3) INS, [ins][POS][inserted_AA(s)], multiple consecutive amino acid insertions are concatenated in order
    * pos input should be 0-indexed
    * nucletide indels may be represented as single AA change + DEL/INS, in this case,
      single AA mutation happens before insertions and after deletions
    '''
    try:
        protein_id = pos_protein_dict[pos]
    except KeyError:
        return

    if abs(len(ref) - len(alt)) % 3 != 0:
        # ignore frame shift
        return
    else:
        nt_mut_length = len(ref) - len(alt)

    aa_mut_label = []
    if nt_mut_length == 0:
        # snps
        previous_aa_count = 0
        for protein_region in protein_pos_dict[protein_id]:
            protein_start, protein_end = protein_region
            if protein_start <= pos < protein_end:
                offset = (pos - protein_start) % 3
                ref_seq = reference.seq[pos - offset: pos - offset + 3]
                ref_aa = seq3(translate(ref_seq))
                alt_seq = ref_seq[:offset] + alt + ref_seq[offset + 1:]
                alt_aa = seq3(translate(alt_seq))
                aa_pos = int((pos - protein_start) / 3) + 1 + previous_aa_count
                aa_mut_label.append(ref_aa + str(aa_pos) + alt_aa)
            previous_aa_count += int((protein_region[1] - protein_region[0]) / 3)
    elif nt_mut_length > 0:
        # inframe deletion
        pos = pos + 1
        previous_aa_count = 0
        for protein_region in protein_pos_dict[protein_id]:
            protein_start, protein_end = protein_region
            if protein_start <= pos and pos + nt_mut_length < protein_end:
                offset = (pos - protein_start) % 3
                ref_seq = reference.seq[pos - offset: pos + nt_mut_length - offset + 3]
                ref_aa = seq3(translate(ref_seq))

                alt_seq = ref_seq[:offset] + ref_seq[offset + nt_mut_length:]
                alt_aa = seq3(translate(alt_seq))

                aa_pos = int((pos - protein_start) / 3) + 1 + previous_aa_count

                aa_length = int(nt_mut_length / 3)

                # print(offset, ref_seq, alt_seq)
                if offset == 0:
                    for i in range(int(nt_mut_length / 3)):
                        aa_mut_label.append((ref_aa[i * 3:(i + 1) * 3] + str(aa_pos + i) + '-'))
                else:
                    if ref_aa[0:3] == alt_aa:
                        for i in range(1, aa_length + 1, 1):
                            aa_mut_label.append((ref_aa[i * 3:(i + 1) * 3] + str(aa_pos + i) + '-'))
                    elif ref_aa[-3:0] == alt_aa:
                        for i in range(aa_length):
                            aa_mut_label.append((ref_aa[i * 3:(i + 1) * 3] + str(aa_pos + i) + '-'))
                    else:
                        for i in range(aa_length):
                            aa_mut_label.append((ref_aa[i * 3:(i + 1) * 3] + str(aa_pos + i) + '-'))
                        aa_mut_label.append((ref_aa[-3:] + str(aa_pos + aa_length) + alt_aa))
            previous_aa_count += int((protein_region[1] - protein_region[0]) / 3)
    else:
        # inframe insertion
        pos = pos
        previous_aa_count = 0
        nt_mut_length = abs(nt_mut_length)

        for protein_region in protein_pos_dict[protein_id]:
            protein_start, protein_end = protein_region
            if protein_start <= pos < protein_end:
                offset = (pos - protein_start) % 3
                ref_seq = reference.seq[pos - offset: pos - offset + 3]
                ref_aa = seq3(translate(ref_seq))
                alt_seq = ref_seq[:offset] + alt + ref_seq[offset + 1:]
                alt_aa = seq3(translate(alt_seq))
                aa_pos = int((pos - protein_start) / 3) + 1 + previous_aa_count

                # print(offset, ref_seq, ref_aa, alt_seq, alt_aa)
                if ref_aa != alt_aa[0:3]:
                    aa_mut_label.append(ref_aa + str(aa_pos) + alt_aa[0:3])
                    aa_mut_label.append("ins" + str(aa_pos) + alt_aa[3:])
                else:
                    aa_mut_label.append("ins" + str(aa_pos) + alt_aa[3:])
            previous_aa_count += int((protein_region[1] - protein_region[0]) / 3)

    return protein_id, aa_mut_label


def query_nt_mut(pos, ref, alt, reference, pos_protein_dict, protein_pos_dict, biocuration_pd_dict):
    '''
    This function takes pos(int), ref(string), alt(string) follows the lofreq output VCF format and returns
      1) df1, the directly associated mutation annotations
      2) df2, supplementary mutation annotations, the annotation that MATCHING position but NOT matching the amino acid change
    '''
    protein_id, mutations = nt_to_aa(pos - 1, ref, alt, reference, pos_protein_dict, protein_pos_dict)

    info_cols = 2

    associated_list = []
    supplementary_list = []
    for mutation in mutations:
        query_series_list = []
        aa_pos = int(re.search('\d+', mutation).group())
        try:
            temp_data = biocuration_pd_dict[protein_id].loc[aa_pos]
            if type(temp_data) == pd.core.frame.DataFrame:
                for idx, row in temp_data.iterrows():
                    query_series_list.append(row[0:info_cols])
            else:
                query_series_list.append(temp_data[0:info_cols])
        except KeyError:
            continue

        biocuration_df = pd.DataFrame(query_series_list).drop_duplicates()

        if mutation.startswith('ins'):
            aa_pos = re.search('\d+', mutation).group()
            aa_inserted = mutation[3 + len(aa_pos):]
            aa_inserted_list = []
            for i in range(0, len(aa_inserted), 3):
                aa_inserted_list.append(aa_inserted[i:i + 3])
            # print(aa_inserted_list)
            for idx, row in biocuration_df.iterrows():
                if ("Insertion" in row[0] or "insertion" in row[0]) and all(x in row[0] for x in aa_inserted_list):
                    associated_list.append(row)
                else:
                    supplementary_list.append(row)

        elif mutation[-1] == "-":
            for idx, row in biocuration_df.iterrows():
                if ("Deletion" in row[0] or "deletion" in row[0]) and mutation[:-1] in row[0]:
                    associated_list.append(row)
                else:
                    supplementary_list.append(row)

        else:
            for idx, row in biocuration_df.iterrows():
                if mutation in row[0]:
                    associated_list.append(row)
                else:
                    supplementary_list.append(row)

    return pd.DataFrame(associated_list).drop_duplicates(), pd.DataFrame(supplementary_list).drop_duplicates(), protein_id, ",".join(mutations)


def test_case(reference, pos_protein_dict, protein_pos_dict, biocuration_pd_dict, pos, ref, alt):
    print(pos, ref, alt)
    print(nt_to_aa(pos - 1, ref, alt, reference, pos_protein_dict, protein_pos_dict))
    df1, df2 = query_nt_mut(pos, ref, alt, reference, pos_protein_dict, protein_pos_dict, biocuration_pd_dict)
    print("Direct Associated Mutation(s):")
    print(df1)
    print("Supplementary Mutation(s):")
    print(df2)
    print()


def run_tests(reference, pos_protein_dict, protein_pos_dict, biocuration_pd_dict):
    # Test Case 1
    test_case(reference, pos_protein_dict, protein_pos_dict, biocuration_pd_dict, pos=28361, ref="GGAGAACGCA", alt="G")
    # Test Case 2
    # test_case(reference, pos_protein_dict, protein_pos_dict, biocuration_pd_dict, pos=6512, ref='AGTT', alt='A')
    # Test Case 3
    # test_case(reference, pos_protein_dict, protein_pos_dict, biocuration_pd_dict, pos=22298, ref='A', alt='Agagattcgtggg')


def foo(biocurated_xlsx_f, reference_fasta_f, data_dir_path):
    reference = SeqIO.read(reference_fasta_f, "fasta")

    # load input files
    biocurated_xlsx = pd.read_excel(biocurated_xlsx_f, sheet_name=None)

    protein_pos_dict = get_proteins_pos_dict(biocurated_xlsx)
    pos_protein_dict = get_pos_protein_dict(protein_pos_dict)

    biocuration_pd_dict = dict()
    for protein_id in protein_pos_dict:
        try:
            aa_mut_df = biocurated_xlsx[protein_id].copy()
            aa_mut_df = aa_mut_df[pd.to_numeric(aa_mut_df['Position'], errors='coerce').notnull()]
            aa_mut_df['Position'] = aa_mut_df['Position'].astype(int)
            aa_mut_df.set_index('Position', inplace=True)
            biocuration_pd_dict[protein_id] = aa_mut_df
        except KeyError:
            continue

    # run_tests(reference, pos_protein_dict, protein_pos_dict, biocuration_pd_dict)

    pos = 28361
    ref = "GGAGAACGCA"
    alt = "G"
    df1, df2 = query_nt_mut(pos, ref, alt, reference, pos_protein_dict, protein_pos_dict, biocuration_pd_dict)
    print("Direct Associated Mutation(s):")
    print(df1)
    print("Supplementary Mutation(s):")
    print(df2)

