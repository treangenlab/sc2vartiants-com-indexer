package com.treangenlab.harvest_variant.sc2variants_com_indexer.pango;

import org.junit.Assert;
import org.junit.Test;

public class PangoTest
{
    @Test
    public void testIsLinage_Null()
    {
        Assert.assertFalse(Pango.isLineage(null));
    }

    @Test
    public void testIsLinage_QDot3()
    {
        Assert.assertTrue(Pango.isLineage("Q.3"));
    }

    @Test
    public void testIsLinage_qDot3()
    {
        Assert.assertFalse(Pango.isLineage("q.3"));
    }

    @Test
    public void testIsLinage_Q_3()
    {
        Assert.assertFalse(Pango.isLineage("Q_3"));
    }

    @Test(expected = NullPointerException.class)
    public void testParse_Null()
    {
        Pango.parse(null);
    }

    @Test
    public void testParse_QDot3()
    {
        Assert.assertEquals(Pango.Q_3, Pango.parse("Q.3"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParse_qDot3()
    {
        Pango.parse("q.3");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParse_Q_3()
    {
        Pango.parse("Q_3");
    }

    @Test
    public void testGetCanonicalName_B()
    {
        Assert.assertEquals("B", Pango.B.getCanonicalName());
    }

    @Test
    public void testGetCanonicalName_B_117()
    {
        Assert.assertEquals("B.1.1.7", Pango.B_1_1_7.getCanonicalName());
    }
}
