package com.treangenlab.harvest_variant.sc2variants_com_indexer.pango;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class PangoProviderCsvDirTest
{
    @Test(expected = NullPointerException.class)
    public void testConstruction_Null()
    {
        new PangoProviderCsvDir(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstruction_NonExist()
    {
        new PangoProviderCsvDir(new File("no-exist.txt"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstruction_NonDir() throws IOException
    {
        File file = File.createTempFile("abc", "tmp");
        file.deleteOnExit();
        new PangoProviderCsvDir(file);
    }
}
