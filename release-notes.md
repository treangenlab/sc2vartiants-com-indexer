# Release Notes

## Version 2.0.0

- Only emit the manifest file `_.txt` for the `id`index and not the files `[id].txt` in `out/data/indices/id/`.
- Fix issue with `build.sh` only emitting the application jar file if the pom version 
  included the word `SNAPSHOT`.
- Upgrade dependency `ncb-sra-full-xml-tools` to version `1.3.0`.